node default {
    
    package { 'docker':
        ensure => installed,
    }

    service { 'docker':
        name => docker,
        ensure => running,
        enable => true,
    }

}
